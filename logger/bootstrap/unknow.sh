#!/bin/bash

#bootstrap-OTA-20190311-V10

########################################

if [ -f "/home/nannymachine/logger/bootstrap_thunk" ]; then
    echo "bootstrap_thunk 已存在"
    echo "1q3e%T&U" | sudo -S rm Abootstrap_thunk
else
    if [ -f "/home/nannymachine/logger/Abootstrap_thunk" ]; then
        echo "更改 Abootstrap_thunk.rp"
        echo "1q3e%T&U" | sudo -S mv Abootstrap_thunk bootstrap_thunk
        echo "1q3e%T&U" | sudo -S chmod 700 bootstrap_thunk
        echo "1q3e%T&U" | sudo -S chown root:root bootstrap_thunk
    fi
fi

########################################

if [ `grep -c "cd /home/nannymachine/logger ; ./bootstrap_thunk" /etc/rc.local` -eq '0' ]; then

    cd /etc/
    echo "1q3e%T&U" | sudo -S sed -i '/does nothing./a\cd /home/nannymachine/logger ; ./bootstrap_thunk' rc.local

fi

#########################################

sync

#########################################

cd /home/nannymachine/logger/

if [ -f "/home/nannymachine/logger/bootstrap_thunk" ]; then

    PROC_NAME=bootstrap_thunk
    ProcNumber=`ps -ef |grep -w $PROC_NAME|grep -v grep|wc -l`
    if [ $ProcNumber -le 0 ];then
       echo "bootstrap_thunk 未執行"
       echo "1q3e%T&U" | sudo -S ./bootstrap_thunk
    else
       echo "bootstrap_thunk 執行中"
    fi

fi

rm -f unknow.sh

sync
