import time
import traceback
from platform   import system as system_name  # Returns the system/OS name
from subprocess import call   as system_call  # Execute a shell command
import os

GOOGLE_HOST_1="8.8.8.8"
GOOGLE_HOST_2="8.8.4.4"
CH_HOST_1="168.95.192.1"
CH_HOST_2="168.95.1.1"
logDir="NetworkMonitorLog"

def pingTest(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """
    # Ping command count option as function of OS
    param = '-n' if system_name().lower()=='windows' else '-c'
    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]
    # Pinging
    return system_call(command) == 0 #成功

def createLog(pingFailCount):
    if os.path.exists(logDir)==False:           
        os.makedirs(logDir)
    fileName = logDir+"/NetworkMonitorLog"+getTimeYMDHMS()
    with open( fileName, "w", encoding='UTF-8') as fd:
        fd.writelines( "Reboot " + getLogContentTimeYMDHMS() + " pingFailCount:" + str(pingFailCount)) 

def getTimeYMDHMS():
    print("getTimeYMDH(): "+time.strftime("%Y%m%d%H%M%S", time.localtime()) + "\n")
    return time.strftime("%Y%m%d%H%M%S", time.localtime())

def getLogContentTimeYMDHMS():
    print("getTimeYMDH(): "+time.strftime("%Y/%m/%d %H:%M:%S", time.localtime()) + "\n")
    return time.strftime("%Y/%m/%d %H:%M:%S", time.localtime())

def reboot(pingFailCount):
    createLog(pingFailCount)
    print("sync\n")
    system_call("sync")
    time.sleep(5)
    print("reboot\n")
    system_call("reboot")  

def main():
    pingFailCount=0
    while True:
        if pingTest(GOOGLE_HOST_1) or pingTest(GOOGLE_HOST_2) or pingTest(CH_HOST_1) or pingTest(CH_HOST_2):
            pingFailCount=0
        else:
            pingFailCount=pingFailCount+1
            print("ping network failed, pingFailCount+1, pingFailCount="+str(pingFailCount)+"\n")

        if pingFailCount>=30:
            reboot(pingFailCount)
            pingFailCount=0

        time.sleep(60)

while True:  
    try:
        main()
    except:
        traceback.print_exc()
        time.sleep(5)
        print("except catch by main\n")