#!/usr/bin/env python3

import io
import sys
import traceback
import json
import os
import urllib.request
import re
import socket
import threading
import time
import datetime
import requests
import shutil
import subprocess
import queue
from Class20190220.NetworkService.PostService import PostService
from Class20190220.NetworkService.MqttService import MqttService
from Class20190220.LogService.LogService import LogService
from Class20190220.BackLog import BackLog
from Class20190220.Util.FileUtil import FileUtil
from Class20190220.Base import Config

try:
    import mobileNetConfig
    print("import mobileNetConfig success \n")
except:
    print("import mobileNetConfig error \n")

def stdout(stream,line):
    stream.write(line)
    stream.flush()

def initDir():
    if os.path.exists(Config.LOG_DIR())==False:
        os.makedirs(Config.LOG_DIR())

def mqttSendLog(mqttClient,log,taskQueue):
    for cpuid in mqttClient.requireIpcLogDict:
        if mqttClient.requireIpcLogDict[cpuid]["sendLogFlag"]=="true" and chkLogIpcId(log,cpuid):
            mqttClient.pub(Config.LOG_DIR()+"/"+cpuid,log,0)
        if mqttClient.requireIpcLogDict[cpuid]["firstGet"]=="true":
            mqttClient.requireIpcLogDict[cpuid]["firstGet"]="false"
            taskQueue.put(BackLog.MqttLogSwitchBackLog(mqttClient,cpuid))

def chkLogIpcId(log,cpuid):
    tokens = log.split(" ")
    if( len(tokens) >= 2 and tokens[1]=="*****" and tokens[2]==cpuid):
        return True
    else:
        return False

def crtlDetermine(taskQueue,tag,line):
    if tag!="":
        taskQueue.put(BackLog.PostAlarmBackLog(tag,line))

def startupOtherProcess():
    if os.path.exists("ppcNannyAppNetworkMonitor.py"):
        subprocess.Popen(["python3", "ppcNannyAppNetworkMonitor.py"])

#main thread
def routinThread(taskQueue):
    logService=LogService()
    #接收UDP並輸出
    inputStream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
    outputStream = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')  
   
    mqttClient=MqttService()
    mqttClient.connect()
    mqttClient.requireIpcLogSub() 
    #log的接收處理
    while True:    
        try:
            line = inputStream.readline()
            stdout( outputStream, line)
            logService.mapPush(line)
            logService.mapComsumer()
            #先關閉CRTL log回傳
            crtlDetermine(taskQueue,logService.crtlLogDetect(line),line)
            mqttSendLog(mqttClient,line,taskQueue)
        except:
            traceback.print_exc()
            print("routinThread except ERROR\n")
            time.sleep(1)

#指派任務用thread
def assignThread(taskQueue):
    #定時發送自身cpuid、ip、ipc列表、post檔案
    #housekeeping
    heartBeat = BackLog.InternalMethod( Config.HEARTBEAT_SLEEP(), lambda: taskQueue.put(BackLog.HeartbeatBackLog()) )
    houseKeeping = BackLog.InternalMethod( Config.HOUSEKEEPING_SLEEP(), lambda: taskQueue.put(BackLog.HousekeepingBackLog()) )
    regularPostFile = BackLog.InternalMethod( Config.REGULAR_POST_FILE_SLEEP(), lambda: taskQueue.put(BackLog.RegularPostFileBackLog()) )
    createIpList = BackLog.InternalMethod( Config.CREATE_IP_LIST_FILE_SLEEP(), lambda: taskQueue.put(BackLog.CreateIpListBackLog()) )
    while True:
        try:
            heartBeat()
            houseKeeping()
            regularPostFile()
            createIpList()
            #print("assignThread run everything OK")
            #不讓thread占滿資源，睡一下
            time.sleep(0.1)
        except:
            traceback.print_exc()
            print("assignThread except ERROR\n")
            time.sleep(1)

#消耗queue，執行任務thread
def netIoThread(taskQueue):
    while True:
        try:
            backlog = taskQueue.get()
            backlog()
        except:
            traceback.print_exc()
            print("netIoThread except ERROR\n")
            time.sleep(1)

def main():
    taskQueue=queue.Queue()
    initDir()
    startupOtherProcess()
    threading.Thread(target=assignThread,args=(taskQueue,)).start()
    threading.Thread(target=netIoThread,args=(taskQueue,)).start()
    routinThread(taskQueue)

while True:  
    try:
        main()
    except:
        traceback.print_exc()
        time.sleep(1)
        print("except catch by main\n")
