import os
import re
import time
import shutil
import json
import hashlib
import subprocess
import traceback

#updater version 20180906

class UpdateProgram():

    #True為測試更新，False為正式
    testMode=False
    otaDir="ota"
    rsyncUrl="ppc@nanny-monitor.paypaycloud.com"
    rsyncDir="/home/ppc/nannyupdate/ota/"
    testRsyncDir="/home/ppc/nannyupdate/testOta/"

    def execRsyncCommand(self):
        rsyncDir=self.rsyncDir
        if self.testMode:
            rsyncDir=self.testRsyncDir
        result = os.system("rsync -avrz --timeout=30 "+self.rsyncUrl+":"+rsyncDir+" "+self.otaDir)
        print("rsync result:"+str(result))
        return result == 0

    #從server同步主程式
    def rsync(self):
        if os.path.exists(self.otaDir)==False:
            os.makedirs(self.otaDir)
        retry=0
        while retry<3:
            if self.execRsyncCommand():
                break
            else:
                retry+=1
            print("rsync error retry:"+str(retry))
    
    #讀取更新清單並比對
    def readList(self):
        updateFlag=False
        if os.path.exists(self.otaDir+"/updateList.json"):
            with open(self.otaDir+"/updateList.json") as file:
                data=json.load(file)
                updateFlag=True
                if data["updateFlag"]!="true" and self.testMode==False:
                    updateFlag=False
                    print("updateFlag False")
                #與本地版本號不同才更新
                if data["version"]==self.getLocalVersion():
                    updateFlag=False
                    print("version False")
                for item in data["list"]:
                    print(item["file"])
                    otaFilePath=self.otaDir+"/"+item["file"]
                    if os.path.exists(otaFilePath):
                        if item["md5sum"]==self.md5(otaFilePath):
                            print(item["file"]+" md5 check success")
                        else:
                            updateFlag=False
                            print("md5 False")
                    else:
                        updateFlag=False
                        print("file exist False")
        print("readList updateFlag:"+str(updateFlag))
        return updateFlag

    #產生md5
    def md5(self,fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        print(hash_md5.hexdigest())
        return hash_md5.hexdigest()

    def getLocalVersion(self):
        version="0.0.0"
        if os.path.exists("updateList.json"):
            with open("updateList.json") as file:
                data=json.load(file)
                version=data["version"]
        print("getLocalVersion:"+version)
        return version

    #更新程式
    def putOTA(self):
        try:
            os.system("rsync -avr "+self.otaDir+"/ .")
            os.system("sync")
        except:
            print("WTF")

    
class KillProcess():

    #用關鍵字搜索執行程式
    def getPid(self,keyword):
        #keyword = "main.sh"  # 設置 grep 的關鍵字  
        ptn = re.compile("\s+")  
        p1 = subprocess.Popen(["ps", "-aux"], stdout=subprocess.PIPE)  
        p2 = subprocess.Popen(["grep", keyword], stdin=p1.stdout, stdout=subprocess.PIPE)   
        output = p2.communicate()[0]  
        lines = output.decode("utf-8").strip().split("\n")  
        print(lines)
        pidList=[]
        for line in lines:
            items=ptn.split(line)
            print("items:")
            print(items)
            if len(items)>1:
                print("items[1]:")
                print(items[1])
                pidList.append(items[1])
        print(pidList)
        return pidList

    #殺掉執行程式
    def killKeyword(self,keyword):
        pidList=self.getPid(keyword)
        for pid in pidList:
            os.system("kill -9 "+pid)

    def killWorkList(self):
        #刪掉檔案名稱中有"ppcNannyApp的任何執行檔，所以之後新增的所有執行檔都必須是ppcNannyApp開頭否則無法更新執行
        self.killKeyword("ppcNannyApp")
        self.killKeyword("UDPListener.jar")

class RebootProcess():

    subprocessDir="/home/nannymachine/logger/"

    #重新啟動程式
    def exeSh(self):
        p1=subprocess.Popen(["java", "-jar", "UDPListener.jar"],cwd=self.subprocessDir,stdout=subprocess.PIPE)
        p2=subprocess.Popen(["python3", "ppcNannyAppLogger.py"],cwd=self.subprocessDir,stdin=p1.stdout)

    def exeUnKnow(self):
        try:
            if os.path.exists("unknow.sh"):
                os.system("sh unknow.sh")
            if os.path.exists("unknow.py"):
                os.system("python3 unknow.py")
        except Exception as e:
            print( e.__doc__ )
            print( traceback.format_exc() )
            print("unknow no execution")

    def exeWorkList(self):
        self.exeUnKnow()
        self.exeSh()


def killAndReboot():
    #關閉
    killer=KillProcess()
    killer.killWorkList()
    #重啟
    rebooter=RebootProcess()
    rebooter.exeWorkList()

def regularUpdate(updater):
    updater.putOTA()
    print("update success")
    killAndReboot()

def startUp():
    print("first startup")
    killAndReboot()

def daemonUpdateRequest():
    while True:
        #更新
        updater=UpdateProgram()
        updater.rsync()
        if updater.readList():        
            regularUpdate(updater)      
        else:
            print("update no execution")

        #每1小時啟動一次
        time.sleep( 1*60*60 )
    
def main():
    startUp()
    daemonUpdateRequest()

while True:
    try:
        main()
    except:
        traceback.print_exc()
        time.sleep(1)
    finally:
        pass
