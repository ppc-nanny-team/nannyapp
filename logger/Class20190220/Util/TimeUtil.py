import time
import os
import datetime

class TimeUtil():

    def timeComputeHour(self,before):
        result=int(self.getTimeYMDH())-int(before)
        if result>=0:
            return result
        else:
            return 0

    def getTimeYMDH(self):
        print("getTimeYMDH(): "+time.strftime("%Y%m%d%H", time.localtime()) )
        return time.strftime("%Y%m%d%H", time.localtime())

    def getTimeYMDHMS(self):
        print("getTimeYMDHMS(): "+time.strftime("%Y%m%d%H%M%S", time.localtime()) )
        return time.strftime("%Y%m%d%H%M%S", time.localtime())

    #取得檔案最新存取時間
    def modifiyDate(self,file):
        if os.path.exists(file):
            t = os.path.getmtime(file)
            return datetime.datetime.fromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')
        else:
            return ""