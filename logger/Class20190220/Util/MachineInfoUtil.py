import os
import re
from Class20190220.Base import Config
from Class20190220.Util.FileUtil import FileUtil
from Class20190220.Util.TimeUtil import TimeUtil

class MachineInfoUtil():

    #取得保母機cpuid
    def getCpuID(self):
        pattern=re.compile(r'[a-z|A-Z|0-9]{20}')
        result=os.popen("cat /proc/cpuinfo")
        lines=result.read()
        m=pattern.search(str(lines))
        cpuid=m.group().upper()
        return cpuid

    def getIpcList(self):
        #建立一個字典dict
        ipcs = []
        for dirName in os.listdir(Config.LOG_DIR()):
            if os.path.isdir(Config.LOG_DIR()+"/"+dirName):
                lastFile=FileUtil().getLastIpcLogFile(dirName)
                #代表ipc log資料夾為空，跳過
                if lastFile!="":
                    #取得檔案最後更新時間
                    lastLogtime = TimeUtil().modifiyDate(lastFile)
                    #取得ipc ip
                    ip=self.getIpcIp(lastFile)
                    #若拿不到ip或是最後更新時間則跳過
                    if ip!="" and lastLogtime!="":
                        #檔案名稱為IPC的ID，將IPC的ID跟檔案最後更新時間寫成一個dict
                        ipc =  {"ipcCpuId": dirName, "lastLogtime": lastLogtime ,"ip":ip,"wifi":""}     
                        #把單一個ipc資料放進ipcs的字典裡面
                        ipcs.append(ipc)
        #print(ipcs)
        return ipcs

    #透過log檔案取得ipc ip
    def getIpcIp(self,file):
        try:
            #2進位檔必須用b
            with open(file, 'rb') as f:
                #設置偏移量
                off = -50      
                while True:
                    f.seek(off, 2) #seek(off, 2)表示文件指針，從檔末尾(2)開始向前50個字元(-50)
                    lines = f.readlines() 
                    if len(lines)>=2: #判斷是否最後至少有兩行，這樣保證了最後一行是完整的
                        lastLine = lines[-1] #取最後一行
                        break
                    #如果off為50時得到的readlines只有一行內容，那麼不能保證最後一行是完整的
                    #所以off翻倍重新運行，直到readlines不止一行
                    off *= 2
                ip=(lastLine.decode().split("[",1)[1]).split("]",1)[0]
                if ip!=None:
                    return ip
                else:
                    return ""
        except:
            return ""