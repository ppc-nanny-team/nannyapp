import os
from Class20190220.Base import Config

class FileUtil():

    def removeStrangeFiles(self):
        for root, dirs, files in os.walk(Config.LOG_DIR()):
            for f in files:
                #單檔大於50MB先移除
                fullpath = os.path.join(root, f)
                if os.path.getsize(fullpath)>50*1000000:
                    os.remove(fullpath)
                    print("single log file bigger than 50MB delete success\n")

    #資料夾容量控制,Query
    def logFileCapacity(self):
        size = 0
        for root, dirs, files in os.walk(Config.LOG_DIR()):
              for f in files:
                fullpath = os.path.join(root, f)
                print(fullpath)
                size+=os.path.getsize(fullpath)
        print("logFileCapacity() size:"+str(size)+"\n")
        return size

    #取得資料夾內最新的log檔案
    def getLastIpcLogFile(self,dirName):
        lastFile=""
        for root, dirs, files in os.walk(Config.LOG_DIR()+"/"+dirName):
            for f in files:
                fullpath = os.path.join(root, f)
                if lastFile=="":
                    lastFile=fullpath
                if int(os.path.getmtime(fullpath))>int(os.path.getmtime(lastFile)):
                    lastFile=fullpath
        return lastFile