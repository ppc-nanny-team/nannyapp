import paho.mqtt.client as mqtt
import io
import sys
import traceback
import time
import json
import re
import os
from Class20190220.Base import Config

class MqttService():
    
    def __init__(self):
        try:
            self.requireIpcLogDict={}
            self.mqttBroker = Config.NANNYMONITOR_MQTT_BROKER()
            self.mqttPort = 1883 #port
            self.mqttClient=mqtt.Client()
        except:
            traceback.print_exc()
            print("ppcNannyAppMqtt() init error\n")

    def connect(self):
        try:
            print("to connect")
            self.mqttClient.on_message=self.on_message
            self.mqttClient.connect(self.mqttBroker, self.mqttPort)
            self.mqttClient.loop_start()
        except:
            traceback.print_exc()
            print("ppcNannyAppMqtt() connect error\n")

    def pub(self,topicName,topicMsg,quality):
        try:
            self.mqttClient.publish( topic=topicName, payload=topicMsg, qos=quality)
        except:
            traceback.print_exc()
            print("ppcNannyAppMqtt() pub error\n")

    def sub(self,topicName):
        try:
            msg=self.mqttClient.subscribe(topicName)
        except:
            traceback.print_exc()
            print("ppcNannyAppMqtt() sub error\n")

    def on_message(self,client, userdata, msg):
        print(msg.payload)
        deMsg=msg.payload.decode("utf8")
        print(deMsg)
        if msg.topic == "hello":
            print("onMessage:",deMsg)
        if msg.topic == "require/"+getCpuID():
            print("on_message receive requireTopic , got you!!")
            self.requireIpcLog(deMsg)

    def requireIpcLogSub(self):
        self.sub("require/"+getCpuID())
        #34007867890C0428074E

    def requireIpcLog(self,deMsg):
        data=json.loads(deMsg)
        if "ipcId" in data and "sendLogFlag" in data:
            if data["ipcId"] not in self.requireIpcLogDict:
                self.requireIpcLogDict[ data["ipcId"] ]=[]  
                property={"sendLogFlag":"true","firstGet":"true"}
                self.requireIpcLogDict[ data["ipcId"] ]=property
            if data["sendLogFlag"] == "true":
                self.requireIpcLogDict[ data["ipcId"] ]["sendLogFlag"]="true"
            elif data["sendLogFlag"] == "false":
                try:
                    del self.requireIpcLogDict[ data["ipcId"] ]
                except:
                    traceback.print_exc()
                    print("requireIpcLog() del requireIpcLogDict[ data[ipcId] ] error\n")
            #ex:requireIpcLogDict={"84007876800C2C2A0A90":["sendLogFlag":"true","firstGet":"true"]}

#取得保母機cpuid
def getCpuID():
    pattern=re.compile(r'[a-z|A-Z|0-9]{20}')
    result=os.popen("cat /proc/cpuinfo")
    lines=result.read()
    m=pattern.search(str(lines))
    cpuid=m.group().upper()
    return cpuid

'''
mqttClient=ppcNannyAppMqtt()
mqttClient.connect()
mqttClient.requireIpcLogSub()
while True:
    reqDict=mqttClient.requireIpcLogDict
    print(reqDict)
    time.sleep(3)
    for cpuid in reqDict:
        if reqDict[cpuid]=="true":
            mqttClient.pub("log/84007876800C2C2A0A90","123")'''

'''
mosquitto_pub -t "require/34007867890C0428074E" -m "{\"ipcId\":\"84007876800C2C2A0A90\",\"sendLogFlag\":\"true\"}"
'''

'''
mosquitto_sub -t "log/84007876800C2C2A0A90"
'''