import json
import os
import time
import traceback
import requests
from Class20190220.Base import Config
from Class20190220.Util.FileUtil import FileUtil
from Class20190220.Util.MachineInfoUtil import MachineInfoUtil

try:
    import mobileNetConfig
    print("import mobileNetConfig success \n")
except:
    print("import mobileNetConfig error \n")

class PostService():
    #post request
    def postReq(self,jsonData,serverUrl):
        try:
            #header json
            headers = {"Content-Type":"application/json","nannyCpuId":MachineInfoUtil().getCpuID()}
            # 將資料加入 POST 
            result=requests.post(serverUrl,data=jsonData,headers=headers,timeout=15)
            #print("postReq OK")
            #print("post return :"+result.content.decode('utf8'))
            print("post "+ serverUrl +" return :"+str(result.status_code)+"\n")
        except:
            traceback.print_exc()      

    #回報該nanny下ipc列表
    def postIpcList(self):
        sendData = {'nannyCpuId': MachineInfoUtil().getCpuID(), 'ipcList': MachineInfoUtil().getIpcList() , 'nannyAppVersion':Config.NANNY_VER()}
        print("postIpcList sendData:")
        print(sendData)
        sendJson = json.dumps(sendData, ensure_ascii=False).encode('utf8')
        self.postReq(sendJson,Config.NANNYMONITOR_IPC_LIST_URL())

    def postAlarm(self,tag,log):
        try:
            ipcCpuId=log.split(" ")[2]
        except:
            ipcCpuId=""
        sendData={"nannyCpuId":MachineInfoUtil().getCpuID(),"ipcCpuId": ipcCpuId,"tag":tag,"content":log}
        print("postAlarm sendData:")
        print(sendData)
        sendJson = json.dumps(sendData, ensure_ascii=False).encode('utf8')
        self.postReq(sendJson,Config.NANNYMONITOR_ALARM_URL())

    #post檔案
    def postFile(self,fullpath,cpuid):
        count=0
        #嘗試3次
        while count<3:
            try:
                headers = {"nannyCpuId":MachineInfoUtil().getCpuID()}
                files={"file":(cpuid,open(fullpath,"rb"))}
                fileResponse = requests.post(Config.NANNYMONITOR_LOG_FILE_URL(),files=files,headers=headers,timeout=30)
                print("post file:"+ fullpath +" return：" + str(fileResponse.status_code)+"\n")
                return True
            except:
                print("post file:" + fullpath +" retry "+str(count)+"\n")
                count+=1
                print("can not post file:"+ fullpath +"\n")
                print( traceback.format_exc() )  
                time.sleep(3)
        return False

    #post log資料夾
    def postLogDirFile(self):
        for root, dirs, files in os.walk(Config.LOG_DIR()):
            for file in files:
                fullpath = os.path.join(root, file)
                print("fullpath:"+fullpath+"\n")
                cpuid=file.split("#",1)[0]
                result=self.postFile(fullpath,cpuid)
                if result:
                    #回傳成功即清空
                    os.remove(fullpath)
                    print("regularPostFile remove "+file+" log success\n")

    def postOneIpcLogDir(self,dirName):
        if os.path.exists("mobileNetConfig.py"):
            if mobileNetConfig.offPostLogFileFlag():
                print("RegularPostFileBackLog() offPostLogFileFlag=True, do not postLogDirFile() \n")
                if os.path.isdir(Config.LOG_DIR()+"/"+dirName):
                    os.remove(Config.LOG_DIR()+"/"+dirName)
            else:
                print("RegularPostFileBackLog() offPostLogFileFlag=False, postLogDirFile() \n")
                oldFile=""
                deleteOneFlag=False
                for root, dirs, files in os.walk(dirName):
                    for file in files:
                        fullpath = os.path.join(root, file)
                        if os.path.exists(oldFile)==False:
                            oldFile=fullpath
                        if int(os.path.getmtime(fullpath))<int(os.path.getmtime(oldFile)):
                            oldFile=fullpath
                        cpuid=file.split("#",1)[0]
                        if self.postFile(fullpath,cpuid)==False:
                            deleteOneFlag=True
                if len(files)>1 and deleteOneFlag:
                    try:
                        os.remove(oldFile)
                        print("postOneIpcLogDir post fail, delete oldFile:"+oldFile+"\n")
                    except:
                        print("postOneIpcLogDir delete oldFile fail\n")

    #log資料夾超過設定大小，無論是否回傳成功皆清空資料夾
    def housekeeping(self):
        FileUtil().removeStrangeFiles()
        if FileUtil().logFileCapacity()>=1000*1000000:
            for dirName in os.listdir(Config.LOG_DIR()):
                if os.path.isdir(Config.LOG_DIR()+"/"+dirName):
                    self.postOneIpcLogDir(dirName)
