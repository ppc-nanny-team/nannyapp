
def NANNY_VER():
    return "1.4.0" 

def NANNYMONITOR():
    return "http://nanny-monitor.paypaycloud.com"

def NANNYMONITOR_IPC_LIST_URL():
    return "http://nanny-monitor.paypaycloud.com:8787/api/receive/87878787"

def NANNYMONITOR_MQTT_BROKER():
    return "nanny-monitor.paypaycloud.com"

def NANNYMONITOR_LOG_FILE_URL():
    return "http://nanny-monitor.paypaycloud.com:8787/api/LogUpload"

def NANNYMONITOR_ALARM_URL():
    return "http://nanny-monitor.paypaycloud.com:8787/api/alarm"

def LOG_DIR():
    return "log"

def HEARTBEAT_SLEEP():
    return 60

def HOUSEKEEPING_SLEEP():
    return 5*60

def REGULAR_POST_FILE_SLEEP():
    return 1*60*60

def CREATE_IP_LIST_FILE_SLEEP():
    return 60