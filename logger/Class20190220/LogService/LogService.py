from Class20190220.Util.TimeUtil import TimeUtil
import os
from Class20190220.Base import Config
import traceback

class LogService():

    def __init__(self):
        self.locMap={}
        self.timeStamp = "0"

    #log條件分析
    def makePair(self,line):
        tokens = line.split(" ")
        #print(tokens)
        if( len(tokens) >= 2 and tokens[1]=="*****" and tokens[2]!="null"):
            return ( tokens[2], " ".join(tokens) )
        else:
            return ( None, None )

    #log放入map dict
    def mapPush(self,line):
        key,value = self.makePair(line)
        if( key!=None and key not in self.locMap ):
            self.locMap[key] = []
        if( key!=None ):
            self.locMap[key].append( value )
    
    #dict長度
    def mapLen(self,map):
        result = 0
        for key in map:
            result = result + len( map[key] )
        return result

    #dict寫入檔案
    def mapWrite(self):
        self.setTimeStamp()
        for key in self.locMap:
            if os.path.exists(Config.LOG_DIR()+"/"+key) and os.path.isdir(Config.LOG_DIR()+"/"+key)==False:
                #過渡到新版需先刪除舊log，因為須建立同名資料夾
                os.remove(Config.LOG_DIR()+"/"+key)
            if os.path.exists(Config.LOG_DIR()+"/"+key)==False:            
                os.makedirs(Config.LOG_DIR()+"/"+key)
            fileName = Config.LOG_DIR()+"/"+key+"/"+key+"#"+self.timeStamp
            with open( fileName, "a", encoding='UTF-8') as fd:
                fd.writelines( self.locMap[key] )       

    #dict長度>=100，寫入檔案並清空
    def mapComsumer(self):
        if( self.mapLen(self.locMap) >= 100 ):
            self.mapWrite()
            self.locMap.clear()

    def setTimeStamp(self):       
        if TimeUtil().timeComputeHour(self.timeStamp)>=1:
            self.timeStamp=TimeUtil().getTimeYMDH()

    def crtlLogDetect(self,line):
        tokens = line.split(" ")
        if len(tokens)>=7:
            if tokens[6]=="CRTL":
                return "CRTL"
            if tokens[6]=="IMPT":
                return "IMPT"
        return ""