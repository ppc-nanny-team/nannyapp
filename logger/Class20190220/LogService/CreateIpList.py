from Class20190220.Util.MachineInfoUtil import MachineInfoUtil
import os

class CreateIpList():

    def __init__(self):
        self.fileName = "ipList"

    def createIpListFile(self):
        ipcList = MachineInfoUtil().getIpcList()
        self.clearOldFile()       
        self.createIpFile(ipcList)

    def clearOldFile(self):
        if os.path.exists(self.fileName):
            os.remove(self.fileName)

    def createIpFile(self, ipcList):
        with open( self.fileName, "a", encoding='UTF-8') as fd:
            for ipc in ipcList:
                fd.writelines( ipc["ip"] + "\n") 