from Class20190220.NetworkService.PostService import PostService
from Class20190220.LogService.CreateIpList import CreateIpList
import time
import os
import traceback

try:
    import mobileNetConfig
    print("import mobileNetConfig success \n")
except:
    print("import mobileNetConfig error \n")

class InternalMethod():
    def __init__( self, timeout, func):
        self.startTime = time.time()
        self.timeout = timeout
        self.func = func
        self.func()

    def __call__(self):
        if( self.isTimeout() ):
            self.startTime=time.time()
            self.func()

    def isTimeout(self):
        return ( time.time() - self.startTime ) >= self.timeout

class HeartbeatBackLog():
    def __call__(self):
        print("HeartbeatBackLog\n")
        postService=PostService()
        postService.postIpcList()

class RegularPostFileBackLog():
    def __call__(self):
        print("RegularPostFileBackLog() \n")
        postService=PostService()
        #非吃到飽網路關閉傳輸log檔案
        if os.path.exists("mobileNetConfig.py"):
            if mobileNetConfig.offPostLogFileFlag():
                print("RegularPostFileBackLog() offPostLogFileFlag=True, do not postLogDirFile() \n")
            else:
                print("RegularPostFileBackLog() offPostLogFileFlag=False, postLogDirFile() \n")
                postService.postLogDirFile()
        else:
            print("RegularPostFileBackLog() no mobileNetConfig.py, postLogDirFile() \n")
            postService.postLogDirFile()
        
class HousekeepingBackLog():
    def __call__(self):
        print("HousekeepingBackLog\n")
        postService=PostService()
        postService.housekeeping()

class PostAlarmBackLog():
    def __init__(self,tag,log):
        self.tag=tag
        self.log=log

    def __call__(self):
        print("PostAlarmBackLog\n")
        postService=PostService()
        postService.postAlarm(self.tag,self.log)

class MqttLogSwitchBackLog():
    def __init__(self,mqttClient,cpuid):
        self.mqttClient=mqttClient
        self.cpuid=cpuid

    def __call__(self):
        print("MqttLogSwitchBackLog\n")
        time.sleep(20)
        try:
            del self.mqttClient.requireIpcLogDict[self.cpuid]
        except:
            traceback.print_exc()
            print("MqttLogSwitchBackLog() del mqttClient.requireIpcLogDict[cpuid] error\n")

class CreateIpListBackLog():
    def __call__(self):
        print("CreateIpListBackLog\n")
        CreateIpList().createIpListFile()
        