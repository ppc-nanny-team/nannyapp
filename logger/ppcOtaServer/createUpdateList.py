import json
import traceback
import hashlib
from os import walk
from os.path import join

class CreateJson():

    #版本號
    version="1.3.2-bootstrap-test01"
    fileList=[]
    #正式或測試
    updateFlag="false"
    otaDir="ota"
    testOtaDir="testOta"

    def pathList(self):
        # 指定要列出所有檔案的目錄
        otaDir = self.otaDir
        if self.updateFlag!="true":
            otaDir=self.testOtaDir
        # 遞迴列出所有檔案的絕對路徑
        for root, dirs, files in walk(otaDir):
            for file in files:
                print("pathList file:"+str(file))
                fullpath = join(root, file)
                print("fullpath:"+fullpath)
                subPath=fullpath.replace(otaDir+"/","",1)
                print("subPath:"+subPath)
                if fullpath!=otaDir+"/updateList.json":
                    self.setList(subPath,self.md5(fullpath))

    #產生md5
    def md5(self,fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        print(hash_md5.hexdigest())
        return hash_md5.hexdigest()

    #產生文件列表
    def setList(self,file,md5sum):
        item={}
        item["file"]=file
        item["md5sum"]=md5sum
        if "list" not in self.fileList:
            self.fileList.append(item)

    #產生更新文件列表檔案
    def createFile(self):
        strData={"version":self.version,"updateFlag":self.updateFlag,"list":self.fileList}
        jsonData=json.dumps(strData,sort_keys = True)
        otaDir=self.otaDir
        if self.updateFlag!="true":
            otaDir=self.testOtaDir
        jsonFile=otaDir+"/updateList.json"
        with open( jsonFile, "w", encoding='UTF-8') as file:
                file.writelines( jsonData ) 


def main():
    creator=CreateJson()
    creator.pathList()
    creator.createFile()

try:
    main()
except Exception as e:
    print( e.__doc__ )
    #print( e.message )
    print( traceback.format_exc() )